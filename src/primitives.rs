use crate::materials::Material;
use crate::math::{Ray, Vec3};
use crate::HitRecord;

pub trait Hitable {
    fn hit(&self, r: Ray, t_min: f64, t_max: f64) -> Option<HitRecord>;
}

pub struct Sphere {
    pub center: Vec3,
    pub radius: f64,
    pub material: Box<Material>,
}

impl Hitable for Sphere {
    fn hit(&self, r: Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        let oc = r.origin - self.center;
        let a = Vec3::dot(r.direction, r.direction);
        let b = Vec3::dot(oc, r.direction);
        let c = Vec3::dot(oc, oc) - self.radius.powi(2);
        let discriminant = b.powi(2) - a * c;
        if discriminant > 0.0 {
            let mut temp = (-b - discriminant.sqrt()) / a;

            if temp < t_max && temp > t_min {
                let hit_point = r.point_at_parameter(temp);

                return Some(HitRecord {
                    t: temp,
                    p: hit_point,
                    normal: (hit_point - self.center) / self.radius,
                    material: self.material.as_ref(),
                });
            }

            temp = (-b + discriminant.sqrt()) / a;

            if temp < t_max && temp > t_min {
                let hit_point = r.point_at_parameter(temp);

                return Some(HitRecord {
                    t: temp,
                    p: hit_point,
                    normal: (hit_point - self.center) / self.radius,
                    material: self.material.as_ref(),
                });
            }
        }

        None
    }
}

pub struct Group {
    pub primitives: Vec<Box<Hitable>>,
}

impl Hitable for Group {
    fn hit(&self, r: Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        let mut temp_rec = None;
        let mut closest_so_far = t_max;

        for primative in &self.primitives {
            if let Some(record) = primative.hit(r, t_min, closest_so_far) {
                closest_so_far = record.t;
                temp_rec = Some(record);
            }
        }

        temp_rec
    }
}
