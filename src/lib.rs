use cameras::Camera;
use materials::{Dielectric, Lambertian, Material, Metal};
use math::{InRange, Ray, Vec3};
use primitives::{Group, Hitable, Sphere};
use rand::prelude::*;
use std::f64;
use std::fs::File;
use std::io::prelude::*;

mod cameras;
mod materials;
mod math;
mod primitives;

pub fn run() {
    let mut file = File::create("image.ppm").expect("Failed to create file");

    let nx = 200;
    let ny = 100;
    let ns = 100;

    let header = format!("P3\n{} {}\n255\n", nx, ny);

    file.write_all(header.as_bytes())
        .expect("Failed to write line");

    let lookfrom = Vec3(13.0, 2.0, 3.0);
    let lookat = Vec3(0.0, 0.5, 0.0);
    let distance_to_focus = (lookfrom - lookat).length();
    let aperature = 0.10;

    let camera = Camera::new(
        lookfrom,
        lookat,
        Vec3(0.0, 1.0, 0.0),
        2.0 * f64::consts::PI / 15.0,
        nx as f64 / ny as f64,
        aperature,
        distance_to_focus,
    );

    let mut rng = rand::thread_rng();

    let world = random_scene(&mut rng);

    for j in (0..ny).rev() {
        for i in 0..nx {
            let mut color = Vec3(0.0, 0.0, 0.0);

            for _ in 0..ns {
                let u = (i as f64 + rng.gen::<f64>()) / nx as f64;
                let v = (j as f64 + rng.gen::<f64>()) / ny as f64;

                let ray = camera.get_ray(u, v, &mut rng);
                color += self::color(ray, &world, 0, &mut rng);
            }

            color /= ns as f64;
            color = Vec3(color.0.sqrt(), color.1.sqrt(), color.2.sqrt());

            let r = (255.99 * color.0) as u8;
            let g = (255.99 * color.1) as u8;
            let b = (255.99 * color.2) as u8;

            let pixel = format!("{} {} {} ", r, g, b);

            file.write_all(pixel.as_bytes())
                .expect("Failed to write pixel");
        }

        file.write_all(b"\n").expect("Failed to write newline");
    }
}

fn color(ray: Ray, world: &Hitable, depth: u32, rng: &mut ThreadRng) -> Vec3 {
    if let Some(record) = world.hit(ray, 0.001, f64::MAX) {
        if let Some((attenuation, scattered)) = record.material.scatter(ray, record, rng) {
            if depth < 50 {
                return attenuation * color(scattered, world, depth + 1, rng);
            }
        }

        Vec3(0.0, 0.0, 0.0)
    } else {
        let unit_direction = Vec3::unit_vector(ray.direction);
        let t = 0.5 * (unit_direction.1 + 1.0);
        (1.0 - t) * Vec3(1.0, 1.0, 1.0) + t * Vec3(0.5, 0.7, 1.0)
    }
}

fn random_scene(rng: &mut ThreadRng) -> impl Hitable {
    let mut primitives: Vec<Box<Hitable>> = Vec::new();

    primitives.push(Box::new(Sphere {
        center: Vec3(0.0, -1000.0, 0.0),
        radius: 1000.0,
        material: Box::new(Lambertian {
            albedo: Vec3(0.5, 0.5, 0.5),
        }),
    }));

    for a in -11..11 {
        for b in -11..11 {
            let choose_mat: f64 = rng.gen();
            let a_offset: f64 = rng.gen();
            let b_offset: f64 = rng.gen();
            let center = Vec3(a as f64 + 0.9 * a_offset, 0.2, b as f64 + 0.9 * b_offset);
            let radius = 0.2;

            if (center - Vec3(4.0, 0.2, 0.0)).length() > 0.9 {
                match choose_mat {
                    x if x.in_range(0.0, 0.8) => {
                        // diffuse
                        let r: f64 = rng.gen();
                        let g: f64 = rng.gen();
                        let b: f64 = rng.gen();
                        primitives.push(Box::new(Sphere {
                            center,
                            radius,
                            material: Box::new(Lambertian {
                                albedo: Vec3(r.powi(2), g.powi(2), b.powi(2)),
                            }),
                        }));
                    }
                    x if x.in_range(0.8, 0.95) => {
                        // metal
                        let r: f64 = rng.gen();
                        let g: f64 = rng.gen();
                        let b: f64 = rng.gen();
                        let fuzz: f64 = rng.gen();
                        primitives.push(Box::new(Sphere {
                            center,
                            radius,
                            material: Box::new(Metal {
                                albedo: Vec3(0.5 * (1.0 + r), 0.5 * (1.0 + g), 0.5 * (1.0 + b)),
                                fuzz: 0.5 * fuzz,
                            }),
                        }));
                    }
                    _ => {
                        // glass
                        primitives.push(Box::new(Sphere {
                            center,
                            radius,
                            material: Box::new(Dielectric {
                                refractive_index: 1.5,
                            }),
                        }))
                    }
                }
            }
        }
    }

    primitives.push(Box::new(Sphere {
        center: Vec3(0.0, 1.0, 0.0),
        radius: 1.0,
        material: Box::new(Dielectric {
            refractive_index: 1.5,
        }),
    }));
    primitives.push(Box::new(Sphere {
        center: Vec3(-4.0, 1.0, 0.0),
        radius: 1.0,
        material: Box::new(Lambertian {
            albedo: Vec3(0.4, 0.2, 0.1),
        }),
    }));
    primitives.push(Box::new(Sphere {
        center: Vec3(4.0, 1.0, 0.0),
        radius: 1.0,
        material: Box::new(Metal {
            albedo: Vec3(0.7, 0.6, 0.5),
            fuzz: 0.0,
        }),
    }));

    Group { primitives }
}

#[derive(Clone, Copy)]
pub struct HitRecord<'a> {
    t: f64,
    p: Vec3,
    normal: Vec3,
    material: &'a Material,
}
