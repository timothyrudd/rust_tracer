use rand::prelude::*;
use std::f64;
use std::ops::{Add, AddAssign, Div, DivAssign, Mul, Neg, Sub};

pub const IOR_AIR: f64 = 1.000277;

pub fn random_in_unit_sphere(rng: &mut ThreadRng) -> Vec3 {
    loop {
        let point = 2.0 * Vec3(rng.gen(), rng.gen(), rng.gen()) - Vec3(1.0, 1.0, 1.0);
        if point.squared_length() >= 1.0 {
            return point;
        }
    }
}

pub fn random_in_unit_disk(rng: &mut ThreadRng) -> Vec3 {
    let theta = 2.0 * f64::consts::PI * rng.gen::<f64>();
    // Random point upto 1 and scale to keep constant area per distance.
    let radius: f64 = rng.gen::<f64>().sqrt();

    Vec3(radius * f64::cos(theta), radius * f64::sin(theta), 0.0)
}

pub fn schlick(cosine: f64, refractive_index: f64) -> f64 {
    let r0 = ((IOR_AIR - refractive_index) / (IOR_AIR + refractive_index)).powi(2);
    r0 + (1.0 - r0) * (1.0 - cosine).powi(5)
}

pub trait InRange {
    fn in_range(&self, begin: Self, end: Self) -> bool;
}

impl InRange for f64 {
    fn in_range(&self, begin: f64, end: f64) -> bool {
        *self >= begin && *self < end
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Vec3(pub f64, pub f64, pub f64);

impl Vec3 {
    pub fn dot(v1: Vec3, v2: Vec3) -> f64 {
        v1.0 * v2.0 + v1.1 * v2.1 + v1.2 * v2.2
    }

    pub fn cross(v1: Vec3, v2: Vec3) -> Vec3 {
        Vec3(
            v1.1 * v2.2 - v1.2 * v2.1,
            v1.2 * v2.0 - v1.0 * v2.2,
            v1.0 * v2.1 - v1.1 * v2.0,
        )
    }

    pub fn unit_vector(v: Vec3) -> Vec3 {
        v / v.length()
    }

    pub fn reflect(vector: Vec3, normal: Vec3) -> Vec3 {
        vector - 2.0 * Vec3::dot(vector, normal) * normal
    }

    pub fn refract(vector: Vec3, normal: Vec3, ni_over_nt: f64) -> Option<Vec3> {
        let uv = Vec3::unit_vector(vector);
        let dt = Vec3::dot(uv, normal);
        let discriminant = 1.0 - ni_over_nt.powi(2) * (1.0 - dt.powi(2));

        if discriminant > 0.0 {
            Some(ni_over_nt * (uv - normal * dt) - normal * discriminant.sqrt())
        } else {
            None
        }
    }

    pub fn length(&self) -> f64 {
        (self.0.powi(2) + self.1.powi(2) + self.2.powi(2)).sqrt()
    }

    fn squared_length(&self) -> f64 {
        self.0.powi(2) + self.1.powi(2) + self.2.powi(2)
    }
}

impl Add for Vec3 {
    type Output = Self;

    fn add(self, other: Vec3) -> Self {
        Vec3(self.0 + other.0, self.1 + other.1, self.2 + other.2)
    }
}

impl AddAssign for Vec3 {
    fn add_assign(&mut self, rhs: Vec3) {
        self.0 += rhs.0;
        self.1 += rhs.1;
        self.2 += rhs.2;
    }
}

impl Sub for Vec3 {
    type Output = Vec3;

    fn sub(self, other: Vec3) -> Vec3 {
        Vec3(self.0 - other.0, self.1 - other.1, self.2 - other.2)
    }
}

impl Mul for Vec3 {
    type Output = Vec3;

    fn mul(self, rhs: Vec3) -> Vec3 {
        Vec3(self.0 * rhs.0, self.1 * rhs.1, self.2 * rhs.2)
    }
}

impl Mul<f64> for Vec3 {
    type Output = Vec3;

    fn mul(self, rhs: f64) -> Self {
        Vec3(self.0 * rhs, self.1 * rhs, self.2 * rhs)
    }
}

impl Mul<Vec3> for f64 {
    type Output = Vec3;

    fn mul(self, rhs: Vec3) -> Vec3 {
        Vec3(self * rhs.0, self * rhs.1, self * rhs.2)
    }
}

impl Div<f64> for Vec3 {
    type Output = Vec3;

    fn div(self, rhs: f64) -> Vec3 {
        Vec3(self.0 / rhs, self.1 / rhs, self.2 / rhs)
    }
}

impl DivAssign<f64> for Vec3 {
    fn div_assign(&mut self, rhs: f64) {
        self.0 /= rhs;
        self.1 /= rhs;
        self.2 /= rhs;
    }
}

impl Neg for Vec3 {
    type Output = Vec3;

    fn neg(self) -> Vec3 {
        Vec3(-self.0, -self.1, -self.2)
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Ray {
    pub origin: Vec3,
    pub direction: Vec3,
}

impl Ray {
    pub fn new(origin: Vec3, direction: Vec3) -> Ray {
        Ray { origin, direction }
    }

    pub fn point_at_parameter(&self, t: f64) -> Vec3 {
        self.origin + t * self.direction
    }
}
