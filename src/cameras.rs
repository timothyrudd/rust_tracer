use crate::math::{random_in_unit_disk, Ray, Vec3};
use rand::prelude::*;

#[derive(Clone, Copy, Debug)]
pub struct Camera {
    origin: Vec3,
    lower_left_corner: Vec3,
    horizontal: Vec3,
    vertical: Vec3,
    u: Vec3,
    v: Vec3,
    w: Vec3,
    lens_radius: f64,
}

impl Camera {
    // vertical_fov is top to bottom in radians.
    pub fn new(
        lookfrom: Vec3,
        lookat: Vec3,
        view_up: Vec3,
        vertical_fov: f64,
        aspect_ratio: f64,
        aperature: f64,
        focus_distance: f64,
    ) -> Camera {
        let theta = vertical_fov;
        let half_height = f64::tan(theta / 2.0);
        let half_width = aspect_ratio * half_height;
        let w = Vec3::unit_vector(lookfrom - lookat);
        let u = Vec3::unit_vector(Vec3::cross(view_up, w));
        let v = Vec3::cross(w, u);

        Camera {
            lower_left_corner: lookfrom
                - half_width * focus_distance * u
                - half_height * focus_distance * v
                - focus_distance * w,
            horizontal: 2.0 * half_width * focus_distance * u,
            vertical: 2.0 * half_height * focus_distance * v,
            origin: lookfrom,
            u,
            v,
            w,
            lens_radius: aperature / 2.0,
        }
    }

    pub fn get_ray(self, s: f64, t: f64, rng: &mut ThreadRng) -> Ray {
        let rd = self.lens_radius * random_in_unit_disk(rng);
        let offset = self.u * rd.0 + self.v * rd.1;
        Ray::new(
            self.origin + offset,
            self.lower_left_corner + s * self.horizontal + t * self.vertical - self.origin - offset,
        )
    }
}
