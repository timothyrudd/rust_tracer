use crate::math::{random_in_unit_sphere, schlick, Ray, Vec3, IOR_AIR};
use crate::HitRecord;
use rand::prelude::*;
use std::f64;

pub trait Material {
    fn scatter(&self, ray: Ray, record: HitRecord, rng: &mut ThreadRng) -> Option<(Vec3, Ray)>;
}

pub struct Lambertian {
    pub albedo: Vec3,
}

impl Material for Lambertian {
    fn scatter(&self, _ray: Ray, record: HitRecord, rng: &mut ThreadRng) -> Option<(Vec3, Ray)> {
        // This algorithm to find a random direction in a hemisphere comes from:
        // http://www.kevinbeason.com/smallpt/.

        // Angle around the hit point.
        let r1 = 2.0 * f64::consts::PI * rng.gen::<f64>();
        // Random point upto 1 and scale to keep constant area per distance.
        let r2: f64 = rng.gen::<f64>().sqrt();

        // let w be the normal.
        let w = record.normal;
        // make a u that is perpendicular to w.
        let u = Vec3::unit_vector(Vec3::cross(
            if w.0.abs() > 0.1 {
                Vec3(0.0, 1.0, 0.0)
            } else {
                Vec3(1.0, 0.0, 0.0)
            },
            w,
        ));
        // make a v that is perpendicular to both w and u.
        let v = Vec3::cross(w, u);

        // Construct the random vector.
        Some((
            self.albedo,
            Ray::new(
                record.p,
                Vec3::unit_vector(
                    u * f64::cos(r1) * r2 + v * f64::sin(r1) * r2 + w * (1.0 - r2).sqrt(),
                ),
            ),
        ))
    }
}

pub struct Metal {
    pub albedo: Vec3,
    pub fuzz: f64,
}

impl Material for Metal {
    fn scatter(&self, ray: Ray, record: HitRecord, rng: &mut ThreadRng) -> Option<(Vec3, Ray)> {
        let reflected = Vec3::reflect(Vec3::unit_vector(ray.direction), record.normal);
        let scattered = Ray::new(record.p, reflected + self.fuzz * random_in_unit_sphere(rng));
        if Vec3::dot(scattered.direction, record.normal) > 0.0 {
            Some((self.albedo, scattered))
        } else {
            None
        }
    }
}

pub struct Dielectric {
    pub refractive_index: f64,
}

impl Material for Dielectric {
    fn scatter(&self, ray: Ray, record: HitRecord, rng: &mut ThreadRng) -> Option<(Vec3, Ray)> {
        let outward_nomal;
        let ni_over_nt;
        let cosine;

        if Vec3::dot(ray.direction, record.normal) > 0.0 {
            outward_nomal = -record.normal;
            ni_over_nt = self.refractive_index / IOR_AIR;
            cosine = self.refractive_index * Vec3::dot(ray.direction, record.normal)
                / ray.direction.length();
        } else {
            outward_nomal = record.normal;
            ni_over_nt = IOR_AIR / self.refractive_index;
            cosine = -Vec3::dot(ray.direction, record.normal) / ray.direction.length();
        }

        let attenuation = Vec3(1.0, 1.0, 1.0);
        let reflected = Some((
            attenuation,
            Ray::new(record.p, Vec3::reflect(ray.direction, record.normal)),
        ));

        if let Some(refracted) = Vec3::refract(ray.direction, outward_nomal, ni_over_nt) {
            // Check if we should reflect or refract.
            if rng.gen::<f64>() < schlick(cosine, self.refractive_index) {
                reflected
            } else {
                Some((attenuation, Ray::new(record.p, refracted)))
            }
        } else {
            reflected
        }
    }
}
